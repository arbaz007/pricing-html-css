let check = document.getElementById("check");
let monthly1 = document.getElementsByClassName("card-price");
check.addEventListener("change", () => {
  console.log("change");
  if (!check.checked) {
    monthly1[0].innerHTML = `<span class="dollar">&dollar;</span>` + 199.99;
    monthly1[1].innerHTML = `<span class="dollar">&dollar;</span>` + 249.99;
    monthly1[2].innerHTML = `<span class="dollar">&dollar;</span>` + 399.99;
  } else {
    monthly1[0].innerHTML = `<span class="dollar">&dollar;</span>` + 19.99;
    monthly1[1].innerHTML = `<span class="dollar">&dollar;</span>` + 24.99;
    monthly1[2].innerHTML = `<span class="dollar">&dollar;</span>` + 39.99;
  }
});
